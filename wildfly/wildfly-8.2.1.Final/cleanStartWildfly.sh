#!/bin/sh
echo ""
echo " This Script will clean wildfly temp directory, updeploy FABS/OTMS/Legal application, and start Wildfly "
echo ""

echo "::: Cleaning B4 Start-----"

echo "      1=> Data"
rm -f -r `dirname $0`/standalone/data/*

echo "      2=> Logs"
echo "          2.1=> WildFly Logs"
rm -f -r `dirname $0`/standalone/log/*
touch    `dirname $0`/standalone/log/server.log

echo "          2.2=> Liferay Logs"
rm -rf `dirname $0`/../logs/*

echo "      3=> Temp"
rm -f -r `dirname $0`/standalone/tmp/*


# remove old deployment entry in standalone-full.xml
echo "      4=> Remove Deployed Applications"
xml ed -P -L -N x="urn:jboss:domain:2.2" -d "//x:deployments" `dirname $0`/standalone/configuration/standalone-full.xml

echo "::: Cleaning Done :::"

deploymentsDir="`dirname $0`/standalone/deployments"
echo ":: Clean .failed deployment files"
for file in  $deploymentsDir/*.failed; do rm -f $file; done
echo ":: Done Cleaning .failed deployment files"
echo ":: Creating dodeploy"
for file in  $deploymentsDir/*/; do touch "`dirname $file`/`basename $file`.dodeploy"; done
echo "::: Done dodeploy :::"

# Start Wildfly
echo "::: Starting Server :::"
#sh -c "`dirname $0`/bin/standalone.sh -c standalone-full.xml -Dorg.jboss.as.logging.per-deployment=false -Dcom.sun.media.jai.disableMediaLib=true -Dlog4j.configurationFile=`dirname $0`/../config/log4j.xml"
#sh -c "nohup `dirname $0`/bin/standalone.sh -c standalone-full.xml -Dcom.sun.media.jai.disableMediaLib=true -Dlog4j.configurationFile=/data/opt/appsrv/wildfly/config/log4j.xml -b 0.0.0.0 &"
