<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%
        String screenName = (String) request.getAttribute("screenName");
        String sendMessage = (String) request.getAttribute("sendMessage");
        String portletInsId = (String) request.getAttribute("portletInsId");
        String UID = (String) request.getAttribute("userID");
        String viewPage = (String) request.getAttribute("viewPage");
        String layoutPLID = (String) request.getAttribute("layoutPLID");
        String waitLoadingS = (String) request.getAttribute("waitLoading");
        if (portletInsId == null || "".equals(portletInsId)){
            portletInsId = screenName + "_WAR_FABSPortal";
        }
        String iframeName = portletInsId+"Frame";
        String screenHeight="100px";
        String screenWidth ="100%";
        boolean waitLoading = false;
        if (waitLoadingS != null && "true".equalsIgnoreCase(waitLoadingS)){
            waitLoading = true;
        }
        String onloadFunction = portletInsId + "resizeIframe();";//
        if (request.getAttribute("screenHeight") != null) {
            screenHeight = (String) request.getAttribute("screenHeight")+"px";
            onloadFunction = "";
        }
        if (!waitLoading)
            onloadFunction += portletInsId + "removeLoadingDiv();";
        
%>
<div id="portletDIV">
    <iframe id="<%=portletInsId%>Frame"
        name="<%=portletInsId%>Frame"
        marginheight="1"  marginwidth="1"
        frameborder="0" width="<%=screenWidth%>"
        height="<%=screenHeight%>" onload="<%=onloadFunction%>">
    </iframe>
    
</div>
<script type="text/javascript">
    /*<![CDATA[*/
    function closeOTMSPortlet(portletDivID){
        var portletDiv = document.getElementById(portletDivID);
        var portletOptions = null;
        Liferay.Portlet.close(portletDiv, true, portletOptions);
    }
    function openOTMSPortlet(screenName, viewPage, pTitle, c2aMode){
        var sortColumns = Liferay.Layout.Columns.sortColumns;
        var plid = themeDisplay.getPlid();
        var targetColumn = '1';
        var placeHolder = jQuery('<div class=\"loading-animation\" />');

        var onComplete = function(data) {
                try{
                    eval(data.portletId+'FrameSrc(\''+viewPage+'\',\''+screenName+'\',\''+c2aMode+'\');');
                    eval(data.portletId+'FrameTitle(\''+pTitle+'\');');
                    jQuery.post( '/FABS-Web/test.iface',
                    {"ajaxaction":"setPortletPre","portletId":data.portletId,
                        "screenName":screenName,"viewPage":viewPage, "pTitle": pTitle},
                            function(responseText){
                            },"html"
                        );                     
                }catch(err){
                    alert(err);
                }
            };
        var beforePortletLoaded = null;

        sortColumns.filter(':eq(' + targetColumn + ')').prepend(placeHolder);

        var portletOptions = {
            beforePortletLoaded: beforePortletLoaded,
            onComplete: onComplete,
            plid: plid,
            portletId: 'OTMSPortlet_WAR_FABS-Portal',
            placeHolder: placeHolder
        };
        var portletInsID = Liferay.Portlet.add(portletOptions);
    }

    Liferay.bind('closePortlet', function(event, data){
        var portletID = data.portletId;
        if (portletID.indexOf('OTMS') != -1){
            removePortletFromServer(portletID);
        }
    });
    
    function removePortletFromServer(portletId){
        jQuery.post( '/FABS-Web/test.iface', 
            {"ajaxaction":"removePortlet","portletId":portletId},
            function(responseText){},"html");
    }

    function <%=portletInsId%>maximizeIframe(iframe) {
		var winHeight = 0;
		if (typeof(window.innerWidth) == 'number') {
			winHeight = window.innerHeight;
		}
		else if ((document.documentElement) &&
				 (document.documentElement.clientWidth || document.documentElement.clientHeight)) {

			winHeight = document.documentElement.clientHeight;
		}
		else if ((document.body) &&
				 (document.body.clientWidth || document.body.clientHeight)) {
			winHeight = document.body.clientHeight;
		}
		iframe.height = (winHeight - 139);
	}

    function <%=portletInsId%>resizeIframe() {
		<%if (request.getAttribute("screenHeight") == null) {%>
        var iframe = document.getElementById('<%=portletInsId%>Frame');

		var height = null;
		var pHeight = 560;

		try {
            
			if (iframe.contentWindow.document.body.scrollHeight < pHeight)
				height = iframe.contentWindow.document.body.scrollHeight;
			else
				height = pHeight;
		}
		catch (e) {
			if (themeDisplay.isStateMaximized()) {
				<%=portletInsId%>maximizeIframe(iframe);
			}
			else {
				iframe.height = pHeight;
			}

			return true;
		}
		iframe.height = height + 20;
        <%}%>
		return true;
	}

	function <%=portletInsId%>updateIframeSize() {
        <%if (request.getAttribute("screenHeight") == null) {%>
		var iframe = document.getElementById('<%=portletInsId%>Frame');
        iframe.height = 150;
        <%=portletInsId%>resizeIframe();
        <%}%>
		return true;
	}
    var <%=portletInsId+"DataBackedUp"%> = false;
    function gotoURL(url){
        var currentURL = themeDisplay.getLayoutURL();
        currentURL = currentURL.substr(0, currentURL.lastIndexOf("/"));
        currentURL = currentURL + url;
        window.location='http://'+location.host+currentURL;
    }
    function <%=portletInsId%>DataRestored(){
        <%=portletInsId%>DataBackedUp = false;
    }

	function <%=portletInsId%>BackupData() {
        try{var frame = jQuery('#<%=portletInsId%>Frame');
        var reB = frame.contents().find('.backupLink');
        reB.click();
        }catch(e){}
    }
    
    function <%=portletInsId%>FrameSrc(viewP, scr, C2Ab){
        var iframeSrc = '/FABS-Web/'+viewP+'?screenName='+scr+'&portletId='+scr+'&UD=<%=UID%>&lPLID=<%=layoutPLID%>&portletInsId=<%=portletInsId%>&portalMode=true&selectFRow=<%=sendMessage%>&EnableC2A='+C2Ab;
        var <%=iframeName%> = jQuery('#<%=iframeName%>');
        <%=iframeName%>.attr('src', iframeSrc );
    }

    function <%=portletInsId%>FrameDefaultSrc(){
        var iframeSrc = '/FABS-Web/<%=viewPage%>?screenName=<%=screenName%>&portletId=<%=screenName%>&UD=<%=UID%>&lPLID=<%=layoutPLID%>&portletInsId=<%=portletInsId%>&portalMode=true&EnableC2A=<%=request.getAttribute("EnableC2A")%>';
        var <%=iframeName%> = jQuery('#<%=iframeName%>');
        try{
            <%=iframeName%>.load(function() {
            <%=portletInsId%>removeLoadingDiv();
            });
        }catch(error){}
        <%=iframeName%>.attr('src', iframeSrc );
    }
    function <%=portletInsId%>FrameTitle(title){
        jQuery('#p_p_id_<%=portletInsId%>_').contents().find('.portlet-title').text(title);
    }
    function <%=portletInsId%>removeLoadingDiv() {
        jQuery('#p_p_id_<%=portletInsId%>_').contents().find('.loading-animation').remove();
    }
    function <%=portletInsId%>StartLoadingDetail(){
        jQuery('iframe').each(function(index, childFrame) {
           try {
               var mainPFrame = '<%=portletInsId%>Frame';
               if (childFrame.name.indexOf(mainPFrame) == -1){
                   eval(childFrame.name+'DefaultSrc();');
               }
           }catch (e){
               alert('Error setting frame src \n' + e);
           }
        });
    }


    jQuery('.sign-out').click(function(event){
        // clear session data on logout.
        jQuery.post( '/FABS-Web/test.iface',
            {"ajaxaction":"logout"},
            function(responseText){},"html");
    });

    jQuery(document).ready(function() {
        jQuery('#p_p_id_<%=portletInsId%>_').contents().find('.portlet-topper').mousedown(function(event){
            <%=portletInsId%>DataBackedUp = true;
            <%=portletInsId%>BackupData();
         });
         var sortColumns = Liferay.Layout.Columns.sortColumns;
         sortColumns.bind( "sortstop.sortable", function(event, ui) {
             if (ui.item[0].portletId.indexOf('<%=portletInsId%>')>-1 ){
                 if (<%=portletInsId%>DataBackedUp == true){
                     var <%=iframeName%> = jQuery('#<%=iframeName%>');
                     var iFsrc =<%=iframeName%>.attr('src') + '&reloadFromSession=true';
                     <%=iframeName%>.attr('src',iFsrc );
                 }
             }
         });
    });
    <%if (request.getAttribute("removeHeader") != null){%>
        jQuery(".portlet-topper").remove();
    <%} %>
    <%if ((!waitLoading )&& (screenName != null && !"".equals(screenName))){%>
        <%=portletInsId%>FrameSrc('<%=viewPage%>', '<%=screenName%>');
    <%}%>
    /*]]>*/
</script>
<div class="loading-animation" />