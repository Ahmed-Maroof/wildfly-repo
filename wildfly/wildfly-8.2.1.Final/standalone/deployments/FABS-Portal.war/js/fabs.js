FABS = {};
FABS.Portlet = {
    staticList: {},
    //TODO : GearBox Work
    //gearOptions:[{}],

    // List of screens that have startLoadingDetailPortlet called for. Is
    // needed to keep track for rendering reasons, to know whether to start
    // load the portlet (if not done before) or to render it afterward
    // TODO: need to cleanup when portlet is closed
    // Note that, calling startLoadingDetailPortlet for the same protlet when
    // being rendered for composite input will clear any messages & session
    // information of previous start load, need to keep them, so, just render
    // instead
    screensStartedLoading: {},
    // List of screens that have render() function called while c2aLink was
    // not yet created, so, it will be called when the screen is built
    screensLateRenderRequest: {},
    //list to save all opened tabs in my window
    openedBrowserTabs: {},
    WebAppContext: undefined,
    sessionTimeoutCounter: 15,
    screenToBeOpened: undefined,
    menu: undefined,
    closed: undefined,
    popupLookup: undefined,
    setWebAppContext: function (contextName) {
        WebAppContext = contextName;
    },
    initialize: function () {
    },
    getPortletTitles: function () {
        console.log('Entering FABS.Portlet.getPortletTitles()');
        var spans = document.getElementsByTagName("span");
        var portletTitles = new Array();
        for (var i = spans.length - 1; i >= 0; i--)
        {
            if (spans[i].className == "portlet-title-text")
                portletTitles.push(spans[i].textContent);
        }
        return portletTitles;
    },
    filterForm: function (form) {
        console.log('Entering FABS.Portlet.filterForm()');
        FABS.Portlet.filterNode(form);
        return form;
    },
    filterNode: function (node) {
        console.log('Entering FABS.Portlet.filterNode()');
        if (node != null)
        {
            //Portlet title addition
            if (node.className == "usr-msg-header-lbl")
            {
                node.textContent = portletTitles.pop();
                node.style.fontWeight = "bold";
                node.style.fontSize = "large";
            }

            //Handle Links
            if (node.tagName == "A")
            {
                var tempLabel = document.createElement("P");
                tempLabel.textContent = node.textContent;
                node.parentNode.insertBefore(tempLabel, node);
                node.parentNode.removeChild(node);
            }

            //Tree node selected adjustment
            if (node.tagName == "LI")
                if (node.getAttribute("aria-checked") == "true")
                {
                    node.style.fontWeight = "bold";
                    node.style.fontStyle = "italic";
                }

            //Paginator spans removal
            if (node.className == "ui-paginator ui-paginator-bottom ui-widget-header")
            {
                var displayedPage = node.childNodes[0].cloneNode(true);
                var td = document.createElement("TD");
                td.colSpan = node.colSpan;
                td.appendChild(displayedPage);
                node.parentNode.insertBefore(td, node);
                node.parentNode.removeChild(node);
            }

            //Button, duplicate TextField, filter, dropdown removal
            if (node.hasChildNodes())
                for (var i = 0; i < node.childNodes.length; i++)
                {
                    if (node.childNodes[i].tagName == "BUTTON"
                            || node.childNodes[i].className == "ui-cell-editor-input"
                            || node.childNodes[i].className == "ui-column-filter ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all"
                            || node.childNodes[i].tagName == "SELECT")
                        node.removeChild(node.childNodes[i]);
                    else
                        FABS.Portlet.filterNode(node.childNodes[i]);
                }
        }
    },
    printPage: function () {
        console.log('Entering FABS.Portlet.printPage()');
        try {
            /*Getting all iframes (except the first in body) children of type form
             Then filtering the form in a new window:
             - Removing Buttons.
             - Displaying the portlet titles.
             - Remove Duplicate column Textfield.
             - Remove filter fields.
             - Adjusting img srcs.
             - Removing unwanted Paginator spans.
             - Remove dropdowns.
             - Replacing links <a> with <p>.
             In addition, to adjusting the CSS.
             */
            var portletFrames = document.getElementsByTagName("iframe");
            newWin = window.open("");
            var tempbody = newWin.document.body;
            portletTitles = FABS.Portlet.getPortletTitles();
            for (var i = 1; i < portletFrames.length; i++)
            {
                var frameChildren = portletFrames[i].contentDocument.body.childNodes;
                for (var j = 0; j < frameChildren.length; j++)
                {
                    if (frameChildren[j].tagName == "FORM")
                    {
                        if (frameChildren[j].id != "buttonsForm")
                        {
                            var filteredForm = FABS.Portlet.filterForm(frameChildren[j].cloneNode(true));
                            tempbody.appendChild(filteredForm);
                        }
                    }
                }
            }
            //Fix Images
            var imgsAfterAppending = tempbody.getElementsByTagName("img");
            for (var k = 0; k < imgsAfterAppending.length; k++)
            {
                imgsAfterAppending[k].src = window.location.protocol + "//" + window.location.host + imgsAfterAppending[k].src;
            }
            //Adjust CSS
            var css = 'form{background: LemonChiffon; border: 3px solid black;}';
            var head = newWin.document.getElementsByTagName('head')[0];
            var style = newWin.document.createElement('style');
            style.type = 'text/css';
            if (style.styleSheet) {
                style.styleSheet.cssText = css;
            } else {
                style.appendChild(newWin.document.createTextNode(css));
            }
            head.appendChild(style);
            //Print
            newWin.print();
            newWin.close();
        } catch (e) {
            if (console)
                console.log(e);
        }
    }, resetSessionTimeoutForAllTabs: function () {
        alert("resetSessionTimeoutForAllTabs")
        sessionTimeoutCounter = 15;
    }, addEvent: function () {
        console.log('Entering FABS.Portlet.addEvent()');
        window.localStorage.count = 1;
    },
    addBrowserTab: function (options) {
        console.log('Entering FABS.Portlet.addBrowserTab()');
        try {
            var currentURL = themeDisplay.getLayoutURL();
            var userrName = options.username;
            currentURL = currentURL.substr(0, currentURL.lastIndexOf("/"));//currentURL = currentURL.substr(0, currentURL.lastIndexOf("/user/"+ userrName+"/"));
            currentURL = currentURL + options.tabURL;
            //var tabURL = 'https://' + location.host + currentURL;
            var tabURL = currentURL;
            //instead of using firefox plugin to add new tab
            var browserType = navigator.userAgent;
            if (browserType.indexOf("Chrome") != -1) {
                var a = window.document.createElement("a");
                a.target = '_blank';// '_blank';
                a.href = tabURL;

                // Dispatch fake click
                var e = window.document.createEvent("MouseEvents");
                e.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, true, false, false, false, 0, null);
                a.dispatchEvent(e);
            } else {
                var instance = this;
                var myWindow = window.open(tabURL, '_blank');//window.open(tabURL,'_blank');
                // myWindow.addEventListener("storage", resetSessionTimeoutForAllTabs,false);
                instance.openedBrowserTabs[tabURL] = myWindow;
            }
        } catch (e) {
            if (console)
                console.log(e);
        }
    },
    closeAllPortletsOnLoad: function () {
        console.log('Entering FABS.Portlet.closeAllPortletsOnLoad()');
        try {
            var portletBodies = document.getElementsByClassName('portlet-body');
            while (portletBodies.length != 0) {
                var portletBody = portletBodies[0];
                var iframe = portletBody.getElementsByTagName('iframe')[0];
                if (iframe == undefined) {
                    var iframe = portletBody.getElementsByTagName('span')[0];
                    var frameId = iframe.getAttribute('id');
                    var portletDivID = 'p_p_id_' + frameId + '_';
                    var portletDiv = document.getElementById(portletDivID);
                    Liferay.Portlet.close(portletDiv, true, {
						p_auth : Liferay.authToken
					});
                } else {
                    var frameId = iframe.getAttribute('id');
                    var portletId = frameId.substr(0, frameId.length - 5);
                    var portletDivID = 'p_p_id_' + portletId + '_';
                    var portletDiv = document.getElementById(portletDivID);
                    Liferay.Portlet.close(portletDiv, true, {
						p_auth : Liferay.authToken
					});
                }

            }
            var instance = this;
            //close all browser tabs
            for (var url in instance.openedBrowserTabs) {
                var myWindow = instance.openedBrowserTabs[url];
                myWindow.close();
            }
            //close parent
            var win = window.opener;
            win.close();

        } catch (e) {
            if (console)
                console.log(e);
        }
    },
    logOut: function () {
        console.log('Entering FABS.Portlet.logOut()');
        try {
            AUI().io.request(
                    WebAppContext + 'test.iface',
                    {
                        data: {
                            "ajaxaction": "signout"
                        },
                        dataType: 'html',
                        on: {
                            success: function (event, id, obj) {
                            }
                        }
                    });
//            var portletBodies = document.getElementsByClassName('portlet-body');
//            while(portletBodies.length != 0) {
//                var portletBody = portletBodies[0];
//                var iframe = portletBody.getElementsByTagName('iframe')[0];
//                var frameId = iframe.getAttribute('id');
//                var portletId = frameId.substr(0, frameId.length - 5);
//                var portletDivID = 'p_p_id_' + portletId + '_';
//                var portletDiv = document.getElementById(portletDivID);
//                Liferay.Portlet.close(portletDiv, true, {});
//            }
            var instance = this;
            //close all browser tabs
            for (var url in instance.openedBrowserTabs) {
                var myWindow = instance.openedBrowserTabs[url];
                myWindow.close();
            }
            //close parent
            var win = window.opener;
            win.close();

        } catch (e) {
            if (console)
                console.log(e);
        }
    },
    addURLBrowserTab: function (options) {
        console.log('Entering FABS.Portlet.addURLBrowserTab()');
        try {
            var tabURL = options.tabURL;

            //instead of using firefox plugin to add new tab
            //instead of using firefox plugin to add new tab
            var browserType = navigator.userAgent;
            if (browserType.indexOf("Chrome") != -1) {
                var a = window.document.createElement("a");
                a.target = 'WWW';
                a.href = tabURL;

                // Dispatch fake click
                var e = window.document.createEvent("MouseEvents");
                e.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, true, false, false, false, 0, null);
                a.dispatchEvent(e);
            } else {
                var instance = this;
                var myWindow = window.open(tabURL, '_blank');
                instance.openedBrowserTabs[tabURL] = myWindow;
            }
            /*
             var element = document.createElement("URLElement");
             element.setAttribute("tabURL", tabURL);
             if (options.selectTab)
             element.setAttribute("selectTab", options.selectTab);
             document.documentElement.appendChild(element);
             //document.getElementById("OTMSDockMenu");
             var evt = document.createEvent("Events");
             evt.initEvent("addFABSPortalPageTabEvent", true, false);
             element.dispatchEvent(evt);
             */
        } catch (e) {
            if (console)
                console.log(e);
        }
    },
    closeBrowserTab: function () {
        console.log('Entering FABS.Portlet.closeBrowserTab()');
        try {
            var element = document.createElement("FABSDataElement");
            element.setAttribute("fabsHost", location.host);
            document.documentElement.appendChild(element);
            var evt = document.createEvent("Events");
            evt.initEvent("closeOneFABSTabEvent", true, false);
            element.dispatchEvent(evt);
        } catch (e) {
            if (console)
                console.log(e);
        }
    },
    lockScrollPosition: function () {
        console.log('Entering FABS.Portlet.lockScrollPosition()');
        var scrollPositionX = self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft;
        var scrollPositionY = self.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
        var html = document.getElementsByTagName('html')[0];

        html.attributes['scroll-positionx'] = scrollPositionX;
        html.attributes['scroll-positiony'] = scrollPositionY;

        window.onscroll = function () {
            var scrollX = html.attributes['scroll-positionx'];
            var scrollY = html.attributes['scroll-positiony'];
            window.scrollTo(scrollX, scrollY);
        };

        setTimeout(function () {
            FABS.Portlet.unlockScrollPosition();
        }, 1500);
    },
    unlockScrollPosition: function () {
        console.log('Entering FABS.Portlet.unlockScrollPosition()');
        window.onscroll = function () {
        };
    },
    shrinkDockMenu: function () {
        console.log('Entering FABS.Portlet.shrinkDockMenu()');
        var mainDiv = document.getElementById("OTMSDockMenu");
        mainDiv.style.height = "";
        mainDiv.style.width = "100%";
        mainDiv.style.right = "10px";
        mainDiv.style.z_index = "100";
        var iframe = document.getElementById("DockMenuFrame");
        iframe.style.height = "";
        iframe.height = "60px";
    },
    shrinkMenuframeSize: function (iframeName) {
        console.log('Entering FABS.Portlet.shrinkMenuframeSize()');
        var iframe = document.getElementById(iframeName);
        iframe.height = 50;
        if (console)
            console.log('Menu Frame ID:  \n' + iframeName);
        return true;
    },
    maxmizeDockMenu: function () {
        console.log('Entering FABS.Portlet.maxmizeDockMenu()');
        var mainDiv = document.getElementById("OTMSDockMenu");
        mainDiv.style.height = "100%";
        mainDiv.style.width = "100%";
        mainDiv.style.right = "10px";
        mainDiv.style.zIndex = "100";
        var iframe = document.getElementById("DockMenuFrame");
        iframe.style.height = "100%";
    },
    overLayMenu: function () {
        console.log('Entering FABS.Portlet.overLayMenu()');
        var banner = document.getElementById("banner");
        banner.style.height = "25px";
        banner.style.display = "block";
        var mainDiv = document.getElementById("OTMSDockMenu");
        mainDiv.style.height = "";
        mainDiv.style.width = "100%";
        mainDiv.style.right = "10px";
        mainDiv.style.zIndex = "100";
        //var mainDiv = document.getElementById("OTMSDockMenu");
        var dockMenuDiv = document.getElementById("p_p_id_DockMenu_WAR_FABSPortal_");
        //alert("dockMenuDiv: "+dockMenuDiv);
        dockMenuDiv.Class = "";
        dockMenuDiv.style.height = "100%";

        var iframe = document.getElementById("DockMenuFrame");
        iframe.height = "60px";
        iframe.style.height = "";
        var spans = iframe.contentDocument.getElementsByClassName("ui-menuitem-icon");
        var span;
        for (i = 0; i < spans.length; i++) {
            //alert(span);
            span = spans[i];
            var idxbeg = span.className.toString().lastIndexOf("(");
            var idxend = span.className.toString().lastIndexOf(")");
            span.style.backgroundImage = "url('" + WebAppContext.toString().substring(0, WebAppContext.toString().length - 1) +
                    span.className.toString().substring(idxbeg + 1, idxend) + "')";//backimg;
            span.style.backgroundSize = "cover";

        }
    },
    close: function (portletId)
    {
        console.log('Entering FABS.Portlet.close()');
        var instance = this;
        var portletDivID = 'p_p_id_' + portletId + '_';
        var A = AUI();
        try {
            var iframeCount = A.all("#" + portletId + "Frame").size();
            if (iframeCount == 0) {
                var portletDiv = document.getElementById(portletDivID);
                var portletOptions = {p_auth: Liferay.authToken};
                Liferay.Portlet.close(portletDiv, true, portletOptions);
            }
        } catch (e) {
            if (console)
                console.log(e);
        }

    },
    closeAllPortlets: function ()
    {
//        var portletBodies = document.getElementsByClassName('portlet-body');
//        while(portletBodies.length != 0) {
//            var portletBody = portletBodies[0];
//            var iframe = portletBody.getElementsByTagName('iframe')[0];
//            var frameId = iframe.getAttribute('id');
//            var portletId = frameId.substr(0, frameId.length - 5);
//            var portletDivID = 'p_p_id_' + portletId + '_';
//            var portletDiv = document.getElementById(portletDivID);
//            Liferay.Portlet.close(portletDiv, true, {});
//        }
    },
    closePopupNoID: function (screenInstId) {
        console.log('Entering FABS.Portlet.closePopupNoID()');
        var dialog = Liferay.Util.Window.getById(screenInstId);
        dialog.destroy();
    },
    closePortletAction: function (portletId)
    {
        console.log('Entering FABS.Portlet.closePortletAction()');
        //alert('2'+portletId);
        var portletDivID = 'p_p_id_' + portletId + '_';
        try {
            var portletDiv = document.getElementById(portletDivID);
            Liferay.Portlet.close(portletDiv, true, {
				p_auth: Liferay.authToken
			});
        }
        catch (e) {
            if (console)
                console.log(e);
        }

    },
    openPortalPage: function (options) {
        console.log('Entering FABS.Portlet.openPortalPage()');
        var funDBID = options.funDBID;
        var userName = options.userName;
        var entityDBID = options.entityDBID;
        var entityType = options.entityType;
        var pageURL = options.pageURL;
        AUI().io.request(
                WebAppContext + 'test.iface',
                {
                    data: {
                        "ajaxaction": "openOrgChart",
                        "funDBID": funDBID,
                        "entityDBID": entityDBID,
                        "entityType": entityType,
                        "userName": userName
                    },
                    dataType: 'html',
                    on: {
                        success: function (event, id, obj) {
                        }
                    }
                });
        try {
            var currentURL = themeDisplay.getLayoutURL();
            currentURL = currentURL.substr(0, currentURL.lastIndexOf("/"));
            currentURL = currentURL + pageURL;
            //var tabURL = 'https://' + location.host + currentURL;
            //var instance = this;
            //var myWindow = window.open(tabURL, '_blank');
            //instance.openedBrowserTabs[tabURL] = myWindow;

        } catch (e) {
            if (console)
                console.log(e);
        }
    },
    open: function (options) {
        //alert("open function is called!");
        console.log('Entering FABS.Portlet.open()');
        var instance = this;
        var A = AUI();
        var screenName = options.screenName;
        var screenInstId = options.screenInstId;
        var screenHeight = options.screenHeight;
        var viewPage = options.viewPage;
        var pTitle = options.portletTitle;
        var enableC2A = options.enableC2A;
        var plid = themeDisplay.getPlid();
        var entityDBID = options.entityDBID;
        var targetColumn = A.one('#layout-column_column-2');
        if (targetColumn == null)
            targetColumn = A.one('#layout-column_column-1');
        var placeHolder = A.Node.create('<div class="loading-animation" />');
        var openedPortlet = instance.isOpended({
            "screenInstId": screenInstId
        }) || "none";
        //if (openedPortlet != 'none'){
        //jQuery.scrollTo("#p_p_id_" + openedPortlet + "_");
        //    var portletDIV = document.getElementById("p_p_id_" + openedPortlet + "_");
        //    var x = portletDIV.offsetLeft;
        //    var y = portletDIV.offsetTop;
        //    window.scrollTo(x, y);
        //} else {
        targetColumn.insert(placeHolder, 0);
        var onComplete = function (portletBoundary, portletId) {
            try {
                // Scroll to THe opened Portlet
                var portletDIV = document.getElementById("p_p_id_" + portletId + "_");
                var x = portletDIV.offsetLeft;
                var y = portletDIV.offsetTop;
                window.scrollTo(x, y);

                instance.setAndSaveTitle({
                    "portletId": portletId,
                    "title": pTitle
                });
                instance.create({
                    "portletId": portletId,
                    "fTitle": screenName
                });
                instance.setOnloadFunction({
                    "portletId": portletId
                });
                instance.setSrc({
                    "portletId": portletId,
                    "viewPage": viewPage,
                    "screenName": screenName,
                    "screenInstId": screenInstId,
                    "screenHeight": screenHeight,
                    "enableC2A": enableC2A

                });
                AUI().io.request(
                        WebAppContext + 'test.iface',
                        {
                            data: {
                                "ajaxaction": "setPortletPre",
                                "portletId": portletId,
                                "screenName": screenName,
                                "screenInstId": screenInstId,
                                "screenHeight": screenHeight,
                                "viewPage": viewPage,
                                "pTitle": pTitle,
                                "entityDBID": entityDBID,
                                "userId": themeDisplay.getUserId()
                            },
                            dataType: 'html',
                            on: {
                                success: function (event, id, obj) {
                                }
                            }
                        });
            } catch (err) {
                if (console)
                    console.log(err);
            }
        };
        var beforePortletLoaded = null;

        var portletOptions = {
            beforePortletLoaded: beforePortletLoaded,
			p_auth: Liferay.authToken,
            onComplete: onComplete,
            plid: plid,
            portletId: 'OTMSPortlet_WAR_FABS-Portal',
            placeHolder: placeHolder
        };
        var portletInsID = Liferay.Portlet.add(portletOptions);
        //}
    },
    openOrgChart: function (options) {
        console.log('Entering FABS.Portlet.openOrgChart()');
        var instance = this;
        var A = AUI();
        var viewPage = options.viewPage;
        var screenInstId = options.screenInstId;
        var entityDBID = options.entityDBID || "";
        var portletHeight = options.portletHeight || 500;
        var portletTitle = options.portletTitle;
        var userName = options.userName;
        var htmlDir = options.htmlDir;
        var fabsWSDLURL = options.fabsWSDLURL;
        var otmsWSDLURL = options.otmsWSDLURL;
        var otmsORGURL = options.otmsORGURL;
        var plid = themeDisplay.getPlid();
        var targetColumn = A.one('#layout-column_column-2');
        if (targetColumn == null)
            targetColumn = A.one('#layout-column_column-1');
        var placeHolder = A.Node.create('<div class="loading-animation" />');
        targetColumn.insert(placeHolder, 0);
        var onComplete = function (portletBoundary, portletId) {
            try {
                // Scroll to THe opened Portlet
                var portletDIV = document.getElementById("p_p_id_" + portletId + "_");
                var x = portletDIV.offsetLeft;
                var y = portletDIV.offsetTop;
                window.scrollTo(x, y);

                instance.setAndSaveTitle({
                    "portletId": portletId,
                    "portletTitle": portletTitle
                });
                instance.create({
                    "portletId": portletId,
                    "fHeight": portletHeight
                });
                instance.setOnloadFunction({
                    "portletId": portletId
                });
                instance.setOrgChartSrc({
                    "portletId": portletId,
                    "screenInstId": screenInstId,
                    "entityDBID": entityDBID,
                    "htmlDir": htmlDir,
                    "userName": userName,
                    "fabsWSDLURL": fabsWSDLURL,
                    "otmsWSDLURL": otmsWSDLURL,
                    "viewPage": viewPage
                });
                var resp = AUI().io.request(
                        WebAppContext + 'test.iface',
                        {
                            data: {
                                "ajaxaction": "setOrgChartPortletPre",
                                "portletId": portletId,
                                "viewPage": viewPage,
                                "portletTitle": portletTitle,
                                "entityDBID": entityDBID,
                                "portletHeight": portletHeight,
                                "htmlDir": htmlDir,
                                "userName": userName,
                                "fabsWSDLURL": fabsWSDLURL,
                                "otmsWSDLURL": otmsWSDLURL,
                                "otmsORGURL": otmsORGURL,
                                "screenInstId": screenInstId
                            },
                            dataType: 'html',
                            on: {
                                success: function (event, id, obj) {
                                }
                            }
                        });

            } catch (err) {
                if (console)
                    console.log(err);
            }
        };
        var beforePortletLoaded = null;

        var portletOptions = {
            beforePortletLoaded: beforePortletLoaded,
            onComplete: onComplete,
            plid: plid,
            portletId: 'EmployeeViewr_WAR_FABS-Portal',
            placeHolder: placeHolder
        };
        var portletInsID = Liferay.Portlet.add(portletOptions);

        //}
    },
    killLiferaySession: function () {
        console.log('Entering FABS.Portlet.killLiferaySession()');
        var A = AUI();
        A.io.request(
                Liferay.Session._sessionUrls.expire,
                {
                    on: {
                        success: function () {
                            Liferay.fire('sessionExpired');
                        }
                    }
                }
        );
    },
    openReport: function (options) {
        console.log('Entering FABS.Portlet.openReport()');
        var instance = this;
        var A = AUI();
        var viewPage = options.viewPage || "Report";
        var reportParams = options.reportParams || "";
        var reportName = options.reportName;
        var reportFormat = options.reportFormat || "HTML";
        var pTitle = options.reportTitle;
        var plid = themeDisplay.getPlid();
        var targetColumn = A.one('#layout-column_column-2');
        if (targetColumn == null)
            targetColumn = A.one('#layout-column_column-1');
        var placeHolder = A.Node.create('<div class="loading-animation" />');
        // Add it to the right column
        targetColumn.insert(placeHolder, 0);
        var onComplete = function (portletBoundary, portletId) {
            try {
                instance.create({
                    "portletId": portletId,
                    "onLoad": "FABS.Portlet.resizeIframe(this);"
                });
                instance.setOnloadFunction({
                    "portletId": portletId
                });
                instance.setReportSrc({
                    "portletId": portletId,
                    "reportName": reportName,
                    "reportFormat": reportFormat,
                    "reportParams": reportParams,
                    "viewPage": viewPage
                });
                instance.setAndSaveTitle({
                    "portletId": portletId,
                    "title": pTitle
                });
                AUI().io.request(
                        WebAppContext + 'test.iface',
                        {
                            data: {
                                "ajaxaction": "setReportPortletPre",
                                "portletId": portletId,
                                "reportName": reportName,
                                "reportFormat": reportFormat,
                                "reportTitle": pTitle
                            },
                            dataType: 'html',
                            on: {
                                success: function (event, id, obj) {
                                }
                            }
                        });
            } catch (err) {
                if (console)
                    console.log(err);
            }
        };
        var beforePortletLoaded = null;

        //        sortColumns.filter(':eq(' + targetColumn + ')').prepend(placeHolder);
        var portletOptions = {
            beforePortletLoaded: beforePortletLoaded,
            onComplete: onComplete,
            plid: plid,
            portletId: 'ReportPortlet_WAR_FABS-Portal',
            placeHolder: placeHolder
        };
        var portletInsID = Liferay.Portlet.add(portletOptions);
    },
    isScreenStartedLoading: function (options) {
        console.log('Entering FABS.Portlet.isScreenStartedLoading()');
        var instance = this;
        var screenInstId = options.screenInstId;
        for (var portlet in instance.screensStartedLoading) {
            if (instance.screensStartedLoading[portlet] == screenInstId) {
                return portlet;
            }
        }
        return undefined;
    },
    isOpended: function (options) {
        console.log('Entering FABS.Portlet.isOpended()');
        var instance = this;
        var screenInstId = options.screenInstId;
        for (var portlet in instance.staticList) {
            if (instance.staticList[portlet] == screenInstId) {
                return portlet;
            }
        }
        return undefined;
    },
    create: function (options) {
        console.log('Entering FABS.Portlet.create()');
        try {
            var A = AUI();
            var iframeName = options.portletId + 'Frame';
            var divName = options.portletId;
            var onloadFunction = options.onLoad || "javascript:;";
            var frameheight = options.fHeight || "100";
            var frameWidth = options.fWidth || "100%";
            var divElem = A.one('#' + divName + '');
            var frameTitle = options.fTitle;
            var tempIFrame = A.Node.create("<iframe name='" + iframeName + "' id='" + iframeName +
                    "' frameborder='0' onload='" + onloadFunction + "' height='"
                    + frameheight + "' width='"
                    + frameWidth + "' title='" + frameTitle + "'></iframe>");
            divElem.replace(tempIFrame);

            // Firefox doesn't use the height attribute passed above, need to pass it again
            tempIFrame.setAttribute('height', frameheight);
        } catch (err) {
            if (console)
                console.log(err);
        }
    },
    createPortletOption: function (options) {

//        try {
//            var A = AUI();
//            var portletId = 'p_p_id_' + options.portletId + '_';
//            var portletOptionElem = A.Node.create("<li />");
//            var aOption = A.Node.create('<a class="taglib-icon" role="menuitem" href="javascript:;" >'+
//                '<img style="background-image: url(\'https://'+
//                location.host +'/html/themes/classic/images/portlet/_sprite.png\'); background-position: 50% -48px; background-repeat: no-repeat; height: 16px; width: 16px;" alt="" src="/html/themes/classic/images/spacer.png" class="icon">' + options.optionsTXT + '</a>');
//            aOption.setAttribute('onclick',
//                    " FABS.Portlet.runScreenOption({actionName:'" + options.actionName + "', portletId:'" + options.portletId + "'}); return false;");
//            var parentUL = A.one('#' + portletId).one('.portlet-topper-toolbar').one('ul li:first-child ul');
//            portletOptionElem.appendChild(aOption);
//            parentUL.appendChild(portletOptionElem);
//        } catch (err) {
//            if (console)
//                console.log(err);
//        }
    },
    createManagePortletOption: function (options) {

        console.log('Entering FABS.Portlet.createManagePortletOption()');
        var instance = this;
        instance.gearOptions.push(options);

        if (instance.gearOptions.length == 7) {
            // alert("calling after = "+instance.gearOptions.length);
            instance.createPortletGearOption();
            // alert("aftercalling  = "+instance.gearOptions.length);
        }
    },
    createReloadPortletOption: function (options) {
        console.log('Entering FABS.Portlet.createReloadPortletOption()');
        try {
            var A = AUI();
            var portletId = 'p_p_id_' + options.portletId + '_';
            var portletOptionElem = A.Node.create("<li />");
            var aOption = A.Node.create('<a class="taglib-icon" role="menuitem" href="javascript:;" ><img style="background-image: url(\'https://' +
                    location.host + '/html/themes/classic/images/portlet/_sprite.png\'); background-position: 50% -48px; background-repeat: no-repeat; height: 16px; width: 16px;" alt="" src="/html/themes/classic/images/spacer.png" class="icon"> Reload</a>');
            aOption.setAttribute('onclick',
                    "AUI().one('#" + options.portletId + "Frame').get('contentWindow.location').reload(true); return false;");
            var parentUL = A.one('#p_p_id_JSFPortlet_WAR_JSFPortletportlet_').
                    one('.portlet-topper-toolbar').
                    one('.portlet-options').one('.direction-down');
            portletOptionElem.appendChild(aOption);
            parentUL.appendChild(portletOptionElem);
        } catch (err) {
            if (console)
                console.log(err);
        }
    },
    reloadPortlet: function (options) {
        console.log('Entering FABS.Portlet.reloadPortlet()');
        AUI().one('#' + options.portletId + 'Frame').get('contentWindow.location').reload(true);
    },
    runScreenOption: function (options) {
        console.log('Entering FABS.Portlet.runScreenOption()');
        try {
            var actionName = options.actionName;
            var portletId = options.portletId;
            var A = AUI();
            var iframe = document.getElementById(portletId + 'Frame');
            var c2aLink = this.findElementWithIdLikeAndTag(iframe.contentWindow, actionName, 'a');
            c2aLink.click();

            //            var iframe = A.one('#'+ portletId + 'Frame');
            //    var c2aLink = iframe.get('contentWindow.document.body').one('a[id$='+actionName+']');
            //           var c2aLink= window.frames['#'+ portletId + 'Frame'].document.getElementById('a[id$='+actionName+']');
            //           c2aLink.click();
            // c2aLink.simulate("click");

        } catch (err) {
            if (console)
                console.log(err);
        }
    },
    runManagePortalPageOption: function (options) {
        console.log('Entering FABS.Portlet.runManagePortalPageOption()');
        try {
            var actionName = options.actionName;
            var portletId = options.portletId;
            var A = AUI();
            var iframe = document.getElementById(portletId + 'Frame');
            var c2aLink = this.findElementWithIdLikeAndTag(iframe.contentWindow, actionName, 'input');
            c2aLink.click();

            //            var iframe = A.one('#'+ portletId + 'Frame');
            //            var c2aLink = iframe.get('contentWindow.document.body').one('input[id$='+actionName+']');
            //            c2aLink.simulate("click");
            if (console)
                console.log("runManagePortalPageOption Function Ran");
        } catch (err) {
            if (console)
                console.log(err);
        }
    },
    runOptionReloadPortletOption: function (options) {
        console.log('Entering FABS.Portlet.runOptionReloadPortletOption()');
        var instance = this;
        instance.setSrc({
            screenName: options.screenName,
            screenInstId: options.screenInstId,
            portletId: options.portletId,
            viewPage: options.viewPage,
            enableC2A: 'fasle'
        })
    },
    setOnloadFunction: function (options) {
        console.log('Entering FABS.Portlet.setOnloadFunction()');
        try {
            var A = AUI();
            var portletId = options.portletId;
            var iframeName = portletId + 'Frame';
            var iframe = A.one('#' + iframeName + '');
            iframe.setAttribute('onload', 'FABS.Portlet.resizeIframe(this);');
        } catch (err) {
            if (console)
                console.log(err);
        }
    },
    setSrc: function (options) {
        console.log('Entering FABS.Portlet.setSrc()');
        try {
            var A = AUI();
            var instance = this;
            var portletId = options.portletId;
            var viewP = options.viewPage;
            var screen = options.screenName;
            var screenInstId = options.screenInstId;
            var screenHeight = options.screenHeight;
            var enC2A = options.enableC2A || "false";
            var selectFR = options.enableC2A || "false";
            var main = options.main || "false";
            var iframeName = portletId + 'Frame';
            var iframeSrc = WebAppContext + viewP
                    + '?screenName=' + screen
                    + '&screenInstId=' + screenInstId
                    + '&screenHeight=' + screenHeight
                    + '&portletId=' + screen
                    + '&UD=' + themeDisplay.getUserId()
                    + '&lPLID=' + themeDisplay.getPlid()
                    + '&portletInsId=' + portletId
                    + '&portalMode=true&selectFRow='
                    + selectFR + '&EnableC2A=' + enC2A
                    + '&main=' + main
            var iframe = A.one('#' + iframeName + '');
            iframe.setAttribute('src', iframeSrc);
            instance.staticList[portletId] = screenInstId;
        } catch (err) {
            if (console)
                console.log(err);
        }
    },
    setReportSrc: function (options) {
        console.log('Entering FABS.Portlet.setReportSrc()');
        try {
            var A = AUI();
            var reportName = options.reportName;
            var reportFormat = options.reportFormat || "HTML";
            var viewPage = options.viewPage || "Report";
            var reportParams = options.reportParams || "";
            var iframeName = options.portletId + 'Frame';

            var iframeSrc = WebAppContext + viewPage + '?report=' + reportName + '&output=' + reportFormat + '&' + reportParams;
            //alert(iframeSrc);
            var iframe = A.one('#' + iframeName + '');
            iframe.setAttribute('src', iframeSrc);
        } catch (err) {
            if (console)
                console.log(err);
        }
    },
    setOrgChartSrc: function (options) {
        console.log('Entering FABS.Portlet.setOrgChartSrc()');
        try {
            var A = AUI();
            var instance = this;
            var portletTitle = options.portletTitle;
            var viewPage = options.viewPage;
            var entityDBID = options.entityDBID;
            var entityName = options.entityName;
            var screenInstId = options.screenInstId;
            var portletId = options.portletId;
            var htmlDir = options.htmlDir;
            var userName = options.userName;
            var fabsWSDLURL = options.fabsWSDLURL;
            var otmsWSDLURL = options.otmsWSDLURL;
            var otmsORGURL = options.otmsORGURL;
            var iframeName = options.portletId + 'Frame';
            var entityActionName = options.entityActionName;

            var iframeSrc = '/FABSOrgChart/faces/' + viewPage + '?entityDBID=' + entityDBID + '&entityName=' + entityName
                    + '&htmlDir=' + htmlDir + '&userName=' + userName + '&fabsWSDLURL=' + fabsWSDLURL + '&otmsWSDLURL=' + otmsWSDLURL
                    + '&entityActionName=' + entityActionName;
            var iframe = A.one('#' + iframeName + '');
            iframe.setAttribute('src', iframeSrc);
            instance.staticList[portletId] = screenInstId;
        } catch (err) {
            if (console)
                console.log(err);
        }
    },
    setTitle: function (options) {
        console.log('Entering FABS.Portlet.setTitle()');
        try {
            var A = AUI();
            var title = options.title;
            var portletId = options.portletId;
            A.one('#p_p_id_' + portletId + '_').all('.portlet-title-text').text(title);
        } catch (err) {
            if (console)
                console.log(err);
        }
    },
    setAndSaveTitle: function (options) {
        console.log('Entering FABS.Portlet.setAndSaveTitle()');
        var instance = this;
        try {
            instance.setTitle(options);
            var title = options.title;
            var portletId = options.portletId;
            var A = AUI();

            var updateURL = themeDisplay.getPathMain() + '/portlet_configuration/update_title'
            var data = {
                doAsUserId: themeDisplay.getDoAsUserIdEncoded(),
                p_l_id: themeDisplay.getPlid(),
                portletId: portletId,
                title: title
            };

            A.io.request(
                    updateURL,
                    {
                        data: data,
                        dataType: 'json',
                        on: {
                            success: null
                        }
                    }
            );
        } catch (err) {
            if (console)
                console.log(err);
        }
    },
    maximizeIframe: function (iframe) {
        console.log('Entering FABS.Portlet.maximizeIframe()');
        var A = AUI();
        var winHeight = 0;
        if (typeof (window.innerWidth) == 'number') {
            winHeight = window.innerHeight;
        }
        else if ((document.documentElement) &&
                (document.documentElement.clientWidth || document.documentElement.clientHeight)) {

            winHeight = document.documentElement.clientHeight;
        }
        else if ((document.body) &&
                (document.body.clientWidth || document.body.clientHeight)) {
            winHeight = document.body.clientHeight;
        }
        iframe.height = (winHeight - 139);
    },
    resizeIframe: function (iframe) {

        console.log('Entering FABS.Portlet.resizeIframe()');
        var instance = this;
        var height = null;
        var pHeight = 560;
        try {
            if (iframe) {
                var iframeWin = iframe.contentWindow || iframe.contentDocument.parentWindow;
                if (iframeWin.document.body) {
                    height = iframeWin.document.documentElement.scrollHeight > iframeWin.document.body.scrollHeight ? iframeWin.document.body.scrollHeight : iframeWin.document.documentElement.scrollHeight;
                    if (height == 0) {//orgchart on firefox height = 0
                        height = 520;
                    }
                    height += 20; //Mohamed Atiea: to avoid vertical scrolling in some pages
                }
                if (iframe.id.indexOf("ReportPortlet") > -1) {
                    height = pHeight;
                }//chrome
            }
            else
                height = pHeight;
        }
        catch (e) {
            if (themeDisplay.isStateMaximized()) {
                instance.maximizeIframe(iframe);
            }
            else {
                iframe.height = pHeight;//firefox
            }
            if (iframe !== null) {
                iframe.height = pHeight;
            }
            return true;
        }
        if (iframe !== null) {
            iframe.height = height;
        }
        return true;

    }, resizeMenuIframe: function (iframe) {
        console.log('Entering FABS.Portlet.resizeMenuIframe()');
        iframe.style.height = 50;
        iframe.height = 50;
        return true;
    },
    updateIframeSizeWithHeight: function (options) {
//        var instance = this;
//        var iframeName = options.iframeName;
//        var iframe = document.getElementById(iframeName);
//        //   alert("iframe = " + iframe);
//
//        var screenHeight = options.screenHeight;
//        if(screenHeight=="null") return;
//        //        iframe.height = 350;
//        //        instance.resizeIframe(iframe);
//        instance.resizeIframeWithHeight({
//            "iframe": iframe,
//            "screenHeight": screenHeight
//        });
//        return true;
    },
    resizeIframeWithHeight: function (options) {
        //   alert("Calling resizeIframeWithHeight");
        console.log('Entering FABS.Portlet.resizeIframeWithHeight()');
        var iframe = options.iframe;
        //    alert("iframe = " + iframe);
        var instance = this;
        var height = null;
        var pHeight = options.screenHeight;
        if (null == pHeight) {
            pHeight = iframe.contentWindow.document.body.scrollHeight;
        }
        try {
//            if (iframe.contentWindow.document.body.scrollHeight < pHeight) {
//                if (iframe.contentWindow.document.body.scrollWidth != iframe.contentWindow.document.width)
//                    height = iframe.contentWindow.document.body.scrollHeight + 20;
//                else
//                    height = iframe.contentWindow.document.body.scrollHeight;
//            }
//            else
//                height = pHeight;
            if (pHeight != null) {
                height = pHeight;
            } else {
                height = iframe.contentWindow.document.body.scrollHeight + 20;
            }
        }
        catch (e) {
            if (themeDisplay.isStateMaximized()) {
                instance.maximizeIframe(iframe);
            }
            else {
                iframe.height = pHeight;
            }
            return true;
        }
        iframe.height = height;
        return true;
    },
    resizeIframeHeightForBalloon: function (options) {
        console.log('Entering FABS.Portlet.resizeIframeHeightForBalloon()');
        var instance = this;
        //  alert("test");
        var iframeName = options.iframeName;
        var iframe = document.getElementById(iframeName);
        //   alert("iframe = " + iframe);
        var screenHeight = options.screenHeight;
        try {
//                height = pHeight;
        }
        catch (e) {
//            if (themeDisplay.isStateMaximized()) {
//                instance.maximizeIframe(iframe);
//            }
//            else {
//                iframe.height = pHeight;
//            }
//            return true;
        }
        iframe.height = screenHeight;
        return true;
    },
    resizeIframeW: function (iframe) {
        console.log('Entering FABS.Portlet.resizeIframeW()');
        var instance = this;
        var width = null;
        var pWidth = 650;

        try {
            if (iframe.contentWindow.document.body.scrollWidth < pHeight) {
                if (iframe.contentWindow.document.body.scrollWidth != iframe.contentWindow.document.width)
                    width = iframe.contentWindow.document.body.scrollWidth + 20;
                else
                    width = iframe.contentWindow.document.body.scrollWidth;
            }
            else
                width = pWidth;
        }
        catch (e) {
            if (themeDisplay.isStateMaximized()) {
                instance.maximizeIframe(iframe);
            }
            else {
                iframe.width = pWidth;
            }
            return true;
        }
        iframe.width = width;
        return true;
    },
    updateIframeSize: function (iframeName) {
        console.log('Entering FABS.Portlet.updateIframeSize()');
        if (iframeName == 'OTMSPortletPopupFrame') {
            return true;
        }
        var instance = this;
        if (iframeName.indexOf("_") == -1) {//normal screen not popup
            iframeName = iframeName.replace("Frame", "");
        }
        var iframe = document.getElementById(iframeName);
        //    iframe.height = 350;
        instance.resizeIframe(iframe);
        return true;
    },
    updateIframeSizeForCal: function (iframeName) {
        console.log('Entering FABS.Portlet.updateIframeSizeForCal()');
        var iframe = document.getElementById(iframeName);
        var currHeight = iframe.height;
        if (currHeight < 350) {
            iframe.height = 350;
        }
        return true;
    },
    updateIframeSizeForCal2: function (iframeName) {
        console.log('Entering FABS.Portlet.updateIframeSizeForCal2()');
        var instance = this;
        var iframe = document.getElementById(iframeName);
        iframe.height = 350;
        instance.resizeIframe(iframe);
        return true;
    },
    updateClosedState: function () {
        console.log('Entering FABS.Portlet.updateClosedState()');
        var iframe = document.getElementById("OTMSDockMenu");
        iframe.height = 50;

    },
    updateSubMenuUlSize: function () {
        console.log('Entering FABS.Portlet.updateSubMenuUlSize()');
        var OTMSDockMenudiv = document.getElementById("OTMSDockMenu");
        //    alert("OTMSDockMenudiv : "+OTMSDockMenudiv.contentWindow);
        var mainUL = OTMSDockMenudiv.contentWindow.getElementById("ID_MegaMenu");
        // alert("mainUL : "+mainUL);
        //
        //             var mainUL= document.getElementById("docMenuForm:ID_MegaMenu").firstChild;
        //            //alert("ID_MegaMenu : "+mainUL);
        //            var liKids =  mainUL.childNodes;
        //            var tdsCount = 0;
        //          		//alert("liKids.length : "+liKids.length);
        //            for(var i=0;i<liKids.length;i++){
        //			var liClassName = liKids[i].className;
        //		//alert(liKids[i].getElementsByTagName("ul"));
        //                var lastUl = liKids[i].getElementsByTagName("ul");
        //                if(lastUl!=null){
        //                    //alert("lastUl : "+lastUl);
        //                    for(var j=0; j<lastUl.length;j++){
        //                        var tds = lastUl[j].getElementsByTagName("td");
        //                        if(tds.length > 1){
        //							var lastULClassName= lastUl[j].className;
        //alert("LI: "+liKids[i]+" UL: "+ lastUl[j]+ "TDs: "+tds.length );
        // var style = document.createElement('style');
        //style.type = 'text/css';
        //style.innerHTML = '.lastUl'+j+' { width:'+(150 * tds.length)+'px!important;}';
        // alert(style.innerHTML);
        // lastUl[j].appendChild(style);
        //alert(lastUl[j].className);
        //lastUl[j].width=(150 * tds.length)+"px!important";
        //alert("width : "+lastUl[j].width)
        //  lastUl[j].style.backgroundColorHover ='blue!important';
        //                         }
        //                     }
        //
        //                 }
        //
        //             }
    },
    updateMenuFrameSize: function () {
        console.log('Entering FABS.Portlet.updateMenuFrameSize()');
        var iframe = document.getElementById("DockMenuFrame");
        var frameSize = iframe.style.height;
        if (frameSize == "50px" || frameSize == "") {
            // iframe.height = "600px";
            iframe.style.height = "800px";
            iframe.zIndex = 2000;
            //closed=false;
        } else {
            // iframe.height = 50;
            iframe.style.height = "50px";
            iframe.zIndex = 0;
            //closed=true;
        }
        return true;
    },
    simulateMouseDblClick: function () {
    },
    gotoURL: function (url) {
        console.log('Entering FABS.Portlet.gotoURL()');
        var currentURL = themeDisplay.getLayoutURL();
        currentURL = currentURL.substr(0, currentURL.lastIndexOf("/"));
        currentURL = currentURL + url;
        window.location = 'https://' + location.host + currentURL;
    },
    removeLoadingDiv: function (portletId) {
        console.log('Entering FABS.Portlet.removeLoadingDiv()');
        var A = AUI();
        A.one('#p_p_id_' + portletId + '_').all('.loading-animation').remove();
    },
    startLoadingDetail: function (mainPortletId) {
        console.log('Entering FABS.Portlet.startLoadingDetail()');
        var A = AUI();
        A.all('.iframeSpan').each(function (childFrame, index) {
            try {
                var mainPFrame = mainPortletId;
                if (childFrame.get('id').indexOf(mainPFrame) == -1) {
                    eval(childFrame.get('id') + 'DefaultSrc();');
                }
            } catch (e) {
                if (console)
                    console.log('Error setting frame src \n' + e);
            }
        });
    },
    ReloadDataOnly: function (options) {
        console.log('Entering FABS.Portlet.ReloadDataOnly()');
        var A = AUI();
        var instance = this;
        var screenInstId = options.screenInstId;
        try {
            // Click on the RDO Link
            var portletId = instance.isOpended({
                "screenInstId": screenInstId
            });
            //            var iframe = A.one('#'+ portletId + 'Frame');
            //            var rdoLink = iframe.get('contentWindow.document.body').query('.rdoLink');
            //            rdoLink.simulate("click");

            var iframe = document.getElementById(portletId + 'Frame');
            var rdoLink = this.findElementWithIdLike(iframe.contentWindow, 'rdoLink');
            rdoLink.click();
        } catch (e) {
        }
    },
    findElementWithIdLikeAndTag: function (wind, prefix, tagName) {
        console.log('Entering FABS.Portlet.findElementWithIdLikeAndTag()');
        return this.findChildWithIdLikeAndTagInner(wind.document.body, prefix, tagName);
    },
    findChildWithIdLikeAndTagInner: function (node, prefix, tagName) {
        console.log('Entering FABS.Portlet.findChildWithIdLikeAndTagInner()');
        if (node && node.id && node.id.indexOf(prefix) >= 0 && node.tagName.toLowerCase() == tagName) {
            //match found
            return node;
        }

        //no match, check child nodes
        for (var index = 0; index < node.childNodes.length; index++) {
            var child = node.childNodes[index];
            var childResult = this.findChildWithIdLikeAndTagInner(child, prefix, tagName);
            if (childResult) {
                return childResult;
            }
        }
    },
    findElementWithIdLike: function (wind, prefix) {
        console.log('Entering FABS.Portlet.findElementWithIdLike()');
        return this.findChildWithIdLike2(wind.document.body, prefix);
    },
    findChildWithIdLike2: function (node, prefix) {
        console.log('Entering FABS.Portlet.findChildWithIdLike2()');
        if (node && node.id && node.id.indexOf(prefix) >= 0) {
            //match found
            return node;
        }

        //no match, check child nodes
        for (var index = 0; index < node.childNodes.length; index++) {
            var child = node.childNodes[index];
            var childResult = this.findChildWithIdLike2(child, prefix);
            if (childResult) {
                return childResult;
            }
        }
    },
    closePortletAndRefreshAnother: function (options) {
        //alert("called");
        //render(options); Mostafa


        console.log('Entering FABS.Portlet.closePortletAndRefreshAnother()');
        var scrn = options.screenName;
        //alert(scrn);
        this.render(options);
        this.closePortletAction(scrn);

    },
    rerenderWindowByC2A: function (contentWindow) {
        console.log('Entering FABS.Portlet.rerenderWindowByC2A()');
        var c2aLink = this.findElementWithIdLike(contentWindow, 'c2aLink');
        if (c2aLink) {
            c2aLink.click();
        }

    }
    ,
    // Is called every time a screen is required to be rendered, regardless of
    // the screen status: whether the screen is created, opened, or not yet.
    renderMyBasic: function (options) {
        console.log('Entering FABS.Portlet.renderMyBasic()');
        var screenInstId = options.screenInstId;
        //var instance = this;
        //alert("screenInstId: "+ screenInstId);
//        var portletID = instance.isScreenStartedLoading({
//                "screenInstId":screenInstId
//            });
        // alert("portletID: " + portletID);
        var iframe = document.getElementById(screenInstId + 'Frame');
        // alert("iframe: "  + iframe);
        this.rerenderWindowByC2A(iframe.contentWindow);
        //alert("Done");
    },
    applyFunctionByPortletId: function (portletId, appliedFunction) {
        console.log('Entering FABS.Portlet.applyFunctionByPortletId()');
        var frames = document.getElementsByTagName("iframe");
        for (var i = 0; i < frames.length; i++) {
            var myWindow = frames[i].contentWindow;
            if (myWindow.document.getElementById(portletId + "marker")) {
                appliedFunction(myWindow)

            }
        }

    },
    renderByPortletId: function (portletId) {
        console.log('Entering FABS.Portlet.renderByPortletId()');
        var that = this;
        this.applyFunctionByPortletId(portletId, function (myWindow) {
            that.rerenderWindowByC2A(myWindow)
        });

    }
    ,
    render: function (options) {
//        alert("called");
        console.log('Entering FABS.Portlet.render()');
        var A = AUI();
        var instance = this;
        var screenInstId = options.screenInstId;
        //alert('1'+screenInstId);
        try {
            // Force Click on the C2a Link
            var portletId_ScreenSL = instance.isScreenStartedLoading({
                "screenInstId": screenInstId
            });
            if (portletId_ScreenSL != undefined) {
                // Portlet is already added to the staticList, meaning, it's creation
                // already has started.
                // Need testing to check it
                // var reB = A.one('#'+ portletId + 'Frame').all('.c2aLink');
                // old code get the frame using Alloy replaced with the code below to achieve cross browser compatability
                //  var iframe = A.one('#'+ portletId_ScreenSL + 'Frame');
                //get the Iframe
                var iframe = document.getElementById(portletId_ScreenSL + 'Frame');
                //find the first element contains the word c2aLink it should be one element
                var c2aLink = this.findElementWithIdLike(iframe.contentWindow, 'c2aLink');
                //Old code using Jquery replaced with the line above using js
                //  var c2aLink = iframe.get('contentWindow.document.body').query('.c2aLink');
                // Validate that c2aLink is valid. It may be invalid if the
                // screen creation is in process and c2aLink is not created yet.
                if (c2aLink == null) {
                    console.error('Null C2ALink: ' + screenInstId +
                            ", putting in screensLateRenderRequest");
                    // Screen must call postBuildInnerScreen after all controls
                    // are created to ensure it's refreshed for this input by
                    // checking the screen in screensLateRenderRequest
                    instance.screensLateRenderRequest[portletId_ScreenSL] = screenInstId;
                }
                else if (c2aLink == undefined) {
                    console.error('Undefined C2ALink: ' + screenInstId +
                            ", putting in screensLateRenderRequest");
                    instance.screensLateRenderRequest[portletId_ScreenSL] = screenInstId;
                }
                else
                    // c2aLink is valid, click it to refresh the screen and process input
                    //changed by @mostafa instead of using alloy simulate to click function becuase simulate not worked with IE in the yahooUI version emebedded in liferay
                    c2aLink.click();
            } else {
                // Portlet is not yet added to screen staticList, means, it needs
                // to be created then rendered, by calling startLoadingDetailPortlet
                instance.startLoadingDetailPortlet({
                    "screenInstId": screenInstId
                });
            }
        } catch (e) {
            if (console)
                console.log('render error\n' + e);
        }
    },
    // Validates if screen is found in screensLateRenderRequest
    doesScreenHaveLateRender: function (options) {
        console.log('Entering FABS.Portlet.doesScreenHaveLateRender()');
        var instance = this;
        var screenInstId = options.screenInstId;
        for (var portlet in instance.screensLateRenderRequest) {
            if (instance.screensLateRenderRequest[portlet] == screenInstId) {
                return portlet;
            }
        }
        return undefined;
    },
    // Screen must call postBuildInnerScreen after all controls are created to
    // ensure it's refreshed if it's in screensLateRenderRequest.
    postBuildInnerScreen: function (options) {
        console.log('Entering FABS.Portlet.postBuildInnerScreen()');
        var A = AUI();
        var instance = this;
        var screenInstId = options.screenInstId;
        try {
            instance.lateRender({
                "screenInstId": screenInstId
            });
        } catch (e) {
            if (console)
                console.log('lateRender error\n' + e);
        }
    },
    lateRender: function (options) {
        console.log('Entering FABS.Portlet.lateRender()');
        var A = AUI();
        var instance = this;
        var screenInstId = options.screenInstId;
        try {
            var portletId = instance.doesScreenHaveLateRender({
                "screenInstId": screenInstId
            });
            if (portletId != undefined) {
                // Remove it from the list to avoid any other late render
                delete instance.screensLateRenderRequest[portletId];

                if (console)
                    console.info('Screen ' + screenInstId + ' Has Late Render');
                //                var iframe = A.one('#'+ portletId_ScreenSL + 'Frame');
                //                var c2aLink = iframe.get('contentWindow.document.body').query('.c2aLink');
                var iframe = document.getElementById(portletId_ScreenSL + 'Frame');
                var c2aLink = this.findElementWithIdLike(iframe.contentWindow, 'c2aLink');
                if (c2aLink == null) {
                    if (console)
                        console.error('lateRender: Null C2ALink: '
                                + screenInstId + ", putting in screensLateRenderRequest");
                }
                else if (c2aLink == undefined) {
                    if (console)
                        console.error('lateRender: Undefined C2ALink: '
                                + screenInstId + ", putting in screensLateRenderRequest");
                }
                else {
                    // c2aLink.simulate("click");
                    c2aLink.click();
                    if (console)
                        console.info('lateRender: C2ALink clicked for ' + screenInstId);
                }
            }
        } catch (e) {
            if (console)
                console.log('lateRender error\n' + e);
        }
    },
    lookupReturned: function (options) {
        //        var A = AUI();
        console.log('Entering FABS.Portlet.lookupReturned()');
        var portletId = options.portletId;
        try {
//                     var iframe = A.one('#'+ portletId + 'Frame');
//                        var c2aLink = iframe.get('contentWindow.document.body').query('.lookupReturned');
//                        c2aLink.simulate("click");
            var iframe = document.getElementById(portletId + 'Frame');
            if (iframe != null) {
                var c2aLink = this.findElementWithIdLike(iframe.contentWindow, 'lookupReturned');
                c2aLink.click();
            } else {
                iframe = document.getElementById(portletId);
                var c2aLink = this.findElementWithIdLike(iframe.children[0].
                        children[1].children[0].contentWindow, 'lookupReturned');
                c2aLink.click();
            }

        } catch (e) {
            if (console)
                console.log('lookupReturned error\n' + e);
        }
    },
    fileEntrtyReturned: function (options) {
        //       var A = AUI();
        console.log('Entering FABS.Portlet.fileEntrtyReturned()');
        var portletId = options.portletId;
        try {
            //            var iframe = A.one('#'+ portletId + 'Frame');
            //            var c2aLink = iframe.get('contentWindow.document.body').query('.fileEntrtyReturned');
            //            c2aLink.simulate("click");
            var iframe = document.getElementById(portletId + 'Frame');

            var c2aLink = this.findElementWithIdLike(iframe.contentWindow, 'fileEntrtyReturned');
            c2aLink.click();

        } catch (e) {
            if (console)
                console.log(e);
        }
    },
    startLoadingDetailPortlet: function (options) {
        console.log('Entering FABS.Portlet.startLoadingDetailPortlet()');
        var instance = this;
        var screenInstId = options.screenInstId;
        var portletId_StaticList = instance.isOpended({
            "screenInstId": screenInstId
        });
        if (portletId_StaticList == undefined) {
            if (console)
                console.log('Error: startLoadingDetailPortlet for undefined screen '
                        + screenInstId + '\n');
        }
        // Add screen instance to list
        instance.screensStartedLoading[portletId_StaticList] = screenInstId;
        try {
            // Clikc on the C2a Link
            var portletId = instance.isOpended({
                "screenInstId": screenInstId
            });
            eval(portletId + 'DefaultSrc();');
        } catch (e) {
            if (console)
                console.log('Error Loading Portlet ' + screenInstId + '\n' + e);
        }
    },
    startLoadingIDPLeft: function (mainPortletId) {
        //        var A = AUI();
        //        var instance = this;
        //        A.one('#column-1').contents().find('.iframeSpan').each(function(index, mChildFrame) {
        //            try {
        //                var mainPFrame = mainPortletId;
        //                if (mChildFrame.id.indexOf(mainPFrame) == -1){
        //                    eval(mChildFrame.id+'DefaultSrc();');
        //                }
        //            }catch (e){
        //                if(console) console.log('Error setting frame src \n ' + e);
        //            }
        //        });
    },
    startLoadingIDPRight: function () {
        //        var instance = this;
        //        jQuery('#column-2').contents().find('.iframeSpan').each(function(index, nChildFrame) {
        //            try {
        //                eval(nChildFrame.id+'DefaultSrc();');
        //            }catch (e){
        //                if(console) console.log('Error setting frame src \n ' + e);
        //            }
        //        });
    },
    restored: function (options) {
        console.log('Entering FABS.Portlet.restored()');
        var instance = this;
    },
    backup: function (options) {
        console.log('Entering FABS.Portlet.backup()');
        var instance = this;
        var portletId = options.portletId;
        try {
            //            var frame = jQuery('#' + portletId + 'Frame');
            //            var reB = frame.contents().find('.backupLink');
            //            reB.click();
        } catch (e) {
            if (console)
                console.log('backup error\n' + e);
        }
    },
    screen: function (options) {
        console.log('Entering FABS.Portlet.screen()');
        var instance = this;
        var portletId = options.portletId;
        var screenInstId;
        try {
            screenInstId = instance.staticList[portletId];
        } catch (e) {
            if (console)
                console.log('screen error\n' + e);
        }
        return screenInstId;
    },
    removeHeader: function () {
        console.log('Entering FABS.Portlet.removeHeader()');
        try {
            var A = AUI();
            A.one(".portlet-topper").remove();
        } catch (error) {
            if (console)
                console.log(error);
        }
    },
    minimize: function (options) {
        console.log('Entering FABS.Portlet.minimize()');
        try {
            var A = AUI();
            var portletId = options.portletId;
            var portletR = A.one('#p_p_id_' + portletId + '_');
            var el = portletR.one('.portlet-minimize .taglib-icon');
            Liferay.Portlet.minimize(portletR, el, {});
        } catch (error) {
            if (console)
                console.log(error);
        }
    },
    openPopup: function (options) {
        console.log('Entering FABS.Portlet.openPopup()');
        var instance = this;
        //var systemURL = options.systemURL;
        var screenName = options.screenName;
        var screenInstId = options.screenInstId;
        var portletInstId = options.portletInstId;
        var viewPage = options.viewPage;
        var lookupTitle = options.title;
        var isLookup = options.isLookup || "false";
        var width = options.width;
        var portletId = screenName;
        //var lPLID = screenName;
        var lPLID = themeDisplay.getPlid();
        var height = 0;
        if (options.height != null) {
            height = options.height;
        }
        //alert(isLookup);

        if (isLookup == "true") {
            lookupTitle = 'Lookup - ' + lookupTitle;
            portletId = 'OTMSPortletPopup';
            lPLID = themeDisplay.getPlid();
        }

        var bodyContentStr =
                "<iframe name='" + portletId + 'Frame'
                + "' id='" + portletId + 'Frame'
                + "' src='" + WebAppContext + viewPage + "?"
                + "screenName=" + screenName
                + "&screenInstId=" + screenInstId
                + "&portletId=" + portletId
                + "&UD=" + themeDisplay.getUserId()
                + "&lPLID=" + lPLID
                + "&portletInsId=" + portletId
                + "&isPopup=true"
                + "&portalMode=true"
                + "&selectFRow=false"
                + "&EnableC2A=false"
                + "&isLookup=" + isLookup
                + "&main=false'"
                + "  frameborder='0'"
                + "  onload='FABS.Portlet.resizeIframe(this);'"
                + "  width='98%' height='98%'/>";
        if (console)
            console.log("openPopup bodyContent=" + bodyContentStr);
        var A = AUI();

//        if (typeof WebAppContext  === 'undefined'){
//            WebAppContext ='/OTMS-Web/';
//        }

//        AUI()
//            .use(
//                'aui-base',
//                'aui-io-plugin-deprecated',
//                'liferay-util-window',
//                'liferay-portlet-url',
//                'aui-dialog-iframe-deprecated',
//                function(A) {
        var url = "" + WebAppContext + viewPage + "?"
                + "screenName=" + screenName
                + "&screenInstId=" + screenInstId
                + "&portletId=" + portletId
                + "&UD=" + themeDisplay.getUserId()
                + "&lPLID=" + lPLID
                + "&portletInsId=" + portletId
                + "&isPopup=true"
                + "&portalMode=true"
                + "&selectFRow=false"
                + "&EnableC2A=false"
                + "&isLookup=" + isLookup
                + "&main=false";
//                    var popUpWindow=Liferay.Util.Window.getWindow(
//                    {
//                        dialog: {
//                            centered: true,
//                            constrain2view: true,
//                            modal: true,
//                            resizable: true
//                        }, id:portletId
//                    }
//                    ).plug(
//                        A.Plugin.DialogIframe,
//                        {
//                            autoLoad: false,
//                            iframeCssClass: 'dialog-iframe',
//                            uri:url.toString()
//                        }).render();
//                    popUpWindow.show();
//                    popUpWindow.titleNode.html(lookupTitle);
//                    popUpWindow.io.start();
//                });
        Liferay.FABS.popup(url, portletId, lookupTitle);

    },
    helpPopup: function (options) {
        //var popup = Liferay.Popup({title: "My Pop",position:[150,150],modal:false,width:800,url: url});
        console.log('Entering FABS.Portlet.helpPopup()');
        var url = options.url;
        var lookupTitle = options.title;
        var portletId = options.screenName;
        Liferay.FABS.popup(url, portletId, lookupTitle);
    },
    openPopupForOrgChart: function (options) {
        console.log('Entering FABS.Portlet.openPopupForOrgChart()');
        var instance = this;
        //var systemURL = options.systemURL;
        var screenName = options.screenName;
        var screenInstId = options.screenInstId;
        var viewPage = options.viewPage;
        var lookupTitle = options.title;
        var isLookup = options.isLookup || "false";
        var width = options.width || 650;
        var portletId = screenName;
        var entityDBID = options.entityDBID;
        var funDBID = options.funDBID;
        //var lPLID = screenName;
        var lPLID = themeDisplay.getPlid();
        var height = 0;
        if (options.height != null) {
            height = options.height;
        }
        //alert(isLookup);

        if (isLookup == "true") {
            portletId = 'OTMSPortletPopup';
            lPLID = themeDisplay.getPlid();
        }

        var bodyContentStr =
                "<iframe name='" + portletId + 'Frame'
                + "' id='" + portletId + 'Frame'
                + "' src='" + WebAppContext + viewPage + "?"
                + "screenName=" + screenName
                + "&screenInstId=" + screenInstId
                + "&portletId=" + portletId
                + "&UD=" + themeDisplay.getUserId()
                + "&lPLID=" + lPLID
                + "&portletInsId=" + portletId
                + "&isPopup=true"
                + "&portalMode=true"
                + "&selectFRow=false"
                + "&EnableC2A=false"
                + "&isOrg=true"
                + "&main=false'"
                + "entityDBID =" + entityDBID
                + "  frameborder='0'"
                + "  onload='FABS.Portlet.resizeIframe(this);'"
                + "  width='98%' height='98%'/>";
        if (console)
            console.log("openPopup bodyContent=" + bodyContentStr);
        if (height > 0) {
            AUI().ready('aui-dialog', function (A) {
                FABS.Portlet.popupLookup = new A.Dialog({
                    bodyContent: bodyContentStr,
                    centered: true,
                    destroyOnClose: true,
                    draggable: true,
                    resizable: true,
                    stack: true,
                    title: lookupTitle,
                    width: width,
                    height: height,
                    modal: false,
                    cssClass: 'clasePrueba',
                    id: 'identificadorDialog'
                }).render();
                FABS.Portlet.staticList[portletId] = screenInstId;
                FABS.Portlet.popupLookup.show();
            });

            AUI().io.request(
                    WebAppContext + 'test.iface',
                    {
                        data: {
                            "ajaxaction": "prepareMsgForOrg",
                            "screenInstId": screenInstId,
                            "entityDBID": entityDBID,
                            "userId": themeDisplay.getUserId(),
                            "funDBID": funDBID
                        },
                        dataType: 'html',
                        on: {
                            success: function (event, id, obj) {
                            }
                        }
                    });

        } else {
            AUI()
                    .use(
                            'aui-base',
                            'aui-io-plugin-deprecated',
                            'liferay-util-window',
                            'liferay-portlet-url',
                            'aui-dialog-iframe-deprecated',
                            function (A) {
                                var url = "" + WebAppContext + viewPage + "?"
                                        + "screenName=" + screenName
                                        + "&screenInstId=" + screenInstId
                                        + "&portletId=" + portletId
                                        + "&UD=" + themeDisplay.getUserId()
                                        + "&lPLID=" + lPLID
                                        + "&portletInsId=" + portletId
                                        + "&isPopup=true"
                                        + "&portalMode=true"
                                        + "&selectFRow=false"
                                        + "&EnableC2A=false"
                                        + "&isOrg=true"
                                        + "&entityDBID=" + entityDBID
                                        + "&funDBID=" + funDBID
                                        + "&isLookup=" + isLookup
                                        + "&main=false"
                                var popUpWindow = Liferay.Util.Window.getWindow(
                                        {
                                            dialog: {
                                                centered: true,
                                                constrain2view: true,
                                                modal: true,
                                                resizable: true
                                            }, id: portletId
                                        }
                                ).plug(
                                        A.Plugin.DialogIframe,
                                        {
                                            autoLoad: false,
                                            iframeCssClass: 'dialog-iframe',
                                            uri: url.toString()
                                        }).render();
                                popUpWindow.show();
                                popUpWindow.titleNode.html("");
                                popUpWindow.io.start();
                            });

        }
        AUI().io.request(
                WebAppContext + 'test.iface',
                {
                    data: {
                        "ajaxaction": "prepareMsgForOrg",
                        "screenInstId": screenInstId,
                        "entityDBID": entityDBID,
                        "userId": themeDisplay.getUserId(),
                        "funDBID": funDBID
                    },
                    dataType: 'html',
                    on: {
                        success: function (event, id, obj) {

                        }
                    }
                });
    },
    fabslogInfo: function (msg) {
        console.info(msg + '\n');
    },
    openMenu: function () {
        console.log('Entering FABS.Portlet.openMenu()');
        var instance = this;
        //check if menu already Opened
        try {
            if (instance.menu != null)
                return;
            var popupWidth = 350;
            var onCloseF = function () {
                instance.menu = null;
                // body.removeClass('lfr-has-sidebar');
            };
            instance.menu = Liferay.Popup(
                    {
                        width: popupWidth,
                        message: '<div class="loading-animation" />',
                        position: [5, 5],
                        resizable: false,
                        title: "OTMS Menu",
                        onClose: onCloseF
                    }
            );
        } catch (error) {
            if (console)
                console.log(error);
        }
    },
    redirectView: function (options) {
        console.log('Entering FABS.Portlet.redirectView()');
        var portletId = options.portletId;
        try {
            var iframe = document.getElementById(portletId + 'Frame');
            if (iframe == null) {
                iframe = document.getElementById(portletId);
            }

            var redirectView = this.findElementWithIdLike(iframe.contentWindow, 'redirectView');
            redirectView.click();
        } catch (e) {
            if (console)
                console.log('lookupReturned error\n' + e);
        }
    },
    buttonTitleAction: function (id, iframeID) {
        console.log('Entering FABS.Portlet.buttonTitleAction()');
        var A = AUI();
        var btnID = id.substring(0, id.indexOf('_butTitle')) + '_button';
        //alert(btnID);
        var iframe = iframeID + 'Frame';
        var title = document.getElementById(iframe).contentDocument.getElementById(btnID);
        //alert(title);
        title.click();
    },
    buttonAction: function (id, iframeID) {
        console.log('Entering FABS.Portlet.buttonAction()');
        var A = AUI();
        var titleID = id.substring(0, id.indexOf('_button')) + '_butTitle';
        var gridID = id.substring(0, id.indexOf('_button')) + '_butPanelGrid';

        var iframe = iframeID + 'Frame';
        var title = document.getElementById(iframe).contentDocument.getElementById(titleID);
        var grd = document.getElementById(iframe).contentDocument.getElementById(gridID);

        title.style.display = '';

        A.one(grd).on('mouseleave', function () {
            //            alert('asdfgh');
            //            alert(iframeID);
            //            alert(titleID);
            //
            //            var title2 = document.getElementById(iframeID).contentDocument.getElementById(titleID);
            //
            //            alert(title2);
            //
            title.style.display = 'none';
        });

    },
    buttonsPanelAction: function (id, iframeID) {
        console.log('Entering FABS.Portlet.buttonsPanelAction()');
        var A = AUI();
        //var absoluteID = id.substring(0, id.indexOf('_butPanelGrid'));

        var btnTitleID = id.substring(0, id.indexOf('_butPanelGrid')) + '_butTitle';
        //var btnID = id.substring(0, id.indexOf('_butPanelGrid'))+'_button';
        //
        //alert(btnID);
        var iframe = iframeID + 'Frame';
        var title = document.getElementById(iframe).contentDocument.getElementById(btnTitleID);
        //alert(title);
        //title.click();

        title.style.display = 'none';

    }
};
FABS.Layout = {
    addLayout: function (options) {
        console.log('Entering FABS.Layout.addLayout()');
        try {
            var layoutTitle = options.layoutTitle || options.lName;
            var A = AUI();
            var updateURL = themeDisplay.getPathMain() + '/layout_management/update_page'

            var data = {
                cmd: 'add',
                doAsUserId: themeDisplay.getDoAsUserIdEncoded(),
                groupId: themeDisplay.getScopeGroupId(),
                mainPath: themeDisplay.getPathMain(),
                name: layoutTitle,
                parentLayoutId: themeDisplay.getParentLayoutId(),
                privateLayout: themeDisplay.isPrivateLayout()
            };

            A.io.request(
                    updateURL,
                    {
                        data: data,
                        dataType: 'json',
                        on: {
                            success: null
                        }
                    }
            );
        } catch (exception) {
            if (console)
                console.log(exception);
        }
    }
};

Liferay.bind('closePortlet', function (data) {
    console.log('Entering Liferay.bind.closePortlet()');
    var portletId = data.portletId;
    var A = AUI();
    if (portletId.indexOf('OTMS') != -1) {
        var screenInstId = FABS.Portlet.screen({
            "portletId": portletId
        });
        try {
            delete FABS.Portlet.staticList[portletId];
        } catch (e) {
            if (console)
                console.log("Can't Delete Object Attribute" + e);
        }
        A.io.request(
                WebAppContext + 'test.iface',
                {
                    data: {
                        "ajaxaction": "removePortlet",
                        "portletId": portletId,
                        "screenInstId": screenInstId
                    },
                    dataType: 'html',
                    on: {
                        success: null
                    }
                });
    }
});
// HBM,New Object to extend Session
FABS.Session = {
    lastLFSessionExtend: 5,
    init: function (options) {
        console.log('Entering FABS.Session.init()');
        // HBM, Timer to decrement lastLFSessionExtend
        var idleInterval = setInterval(FABS.Session.timerDecrement, 60000);
    },
    extend: function (options) {
        console.log('Entering FABS.Session.extend()');
        try {
            if (FABS.Session.lastLFSessionExtend == 0) {
                FABS.Session.lastLFSessionExtend = 5;
                Liferay.Session.extend();
            }
        } catch (exception) {
            if (console)
                console.log(exception);
        }
    },
    timerDecrement: function (options) {
        console.log('Entering FABS.Session.timerDecrement()');
        try {
            if (FABS.Session.lastLFSessionExtend > 0) {
                FABS.Session.lastLFSessionExtend = FABS.Session.lastLFSessionExtend - 1;
            }
        } catch (exception) {
            if (console) {
                console.log(exception)
            }
            ;
        }
    },
    logout: function () {
        console.log('Entering FABS.Session.logout()');
        window.location.replace(themeDisplay.getPortalURL() + '/c/portal/logout');
    }
};
// Call init the first time this file is loaded
FABS.Session.init();

Liferay.bind('allPortletsReady', function (data) {
    console.log('Entering Liferay.bind.allPortletsReady()');
    var A = AUI();
    A.one('.sign-out').on('click', function (event) {
        // Close all the opened tabs on signout.
        /*
         var element = document.createElement("FABSDataElement");
         element.setAttribute("fabsHost", location.host);
         document.documentElement.appendChild(element);
         var evt = document.createEvent("Events");
         evt.initEvent("closeFABSTabsEvent", true, false);
         element.dispatchEvent(evt);
         */
        // Close all layouts on logout
        A.io.request(
                WebAppContext + 'test.iface',
                {
                    data: {
                        "ajaxaction": "closeLiferayLayouts"
                    },
                    dataType: 'html',
                    on: {
                        success: null
                    }
                }
        );
        // clear session data on logout.
        A.io.post(
                WebAppContext + 'test.iface',
                {
                    data: {
                        "ajaxaction": "signout"
                    },
                    dataType: 'html',
                    on: {
                        success: null
                    }
                }
        );
    });
});


Liferay.namespace('FABS');

Liferay.provide(
        Liferay.FABS,
        'popup',
        function (url, portletId, lookupTitle) {
            console.log('Entering Liferay.provide.popup()');
            var A = AUI();
            var popUpWindow = Liferay.Util.Window.getWindow(
                    {
                        dialog: {
                            centered: true,
                            constrain2view: true,
                            modal: true,
                            resizable: true
                        }, id: portletId
                    }
            ).plug(
                    A.Plugin.DialogIframe,
                    {
                        autoLoad: false,
                        iframeCssClass: 'dialog-iframe',
                        uri: url.toString()
                    }).render();
            popUpWindow.show();
            popUpWindow.titleNode.html(lookupTitle);
            popUpWindow.io.start();

        },
        ['aui-io-plugin-deprecated', 'liferay-util-window']
        );
